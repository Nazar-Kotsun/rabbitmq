﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using RabbitMQ.Client.MessagePatterns;

namespace RabbitMQ.Wrapper
{
    public interface IWrapper
    {
        void ListenQueue();
        void SendMessageToQueue(string message, string nameQueue, int delay);
    }

    public class Wrapper : IWrapper, IDisposable
    {
        private ConnectionFactory factory;
        private IConnection connection;
        private IModel channel;
        private Subscription subscription;
        private static object locker = new object();

        public Wrapper(string userName, string password, string hostName, string queueName)
        {
            InitializationWrapper(userName, password, hostName, queueName);
        }
        private void InitializationWrapper(string userName, string password, string hostName, string queueName)
        {
            try
            {
                factory = new ConnectionFactory();
                factory.UserName = userName;
                factory.Password = password;
                factory.HostName = hostName;

                connection = factory.CreateConnection();
                channel = connection.CreateModel();

                channel.QueueDeclare(queueName, false, false, true, null);

                subscription = new Subscription(channel, queueName, false);
                Console.WriteLine(queueName + " created successfully!");
            }
            catch (BrokerUnreachableException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("If you want to exit press Escape");
            }
        }
        
        public void Dispose()
        {
            channel?.Dispose();
            connection?.Dispose();
        }

        public void ListenQueue()
        {
            try
            {
                if (channel != null)
                {
                    BasicDeliverEventArgs basicDeliveryEventArgs = subscription.Next();
                    lock (locker)
                    {
                        if (basicDeliveryEventArgs != null)
                        {
                            string messageContent = Encoding.UTF8.GetString(basicDeliveryEventArgs.Body.ToArray());
                            Console.WriteLine(DateTime.Now + ": " + messageContent);
                            subscription.Ack(basicDeliveryEventArgs);
                        }
                    }
                }
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void SendMessageToQueue(string message, string nameQueueReceiver, int delay)
        {
            try
            {
                if (channel != null)
                {
                    lock (locker)
                    {
                        if (delay < 0)
                        {
                            Console.WriteLine("delay < 0, The default delay is 2.5 seconds.");
                            delay = 2500;
                        }

                        Thread.Sleep(delay);
                        byte[] buffer = Encoding.UTF8.GetBytes(message);
                        channel.BasicPublish(string.Empty, nameQueueReceiver, null, buffer);
                    }
                }
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
} 