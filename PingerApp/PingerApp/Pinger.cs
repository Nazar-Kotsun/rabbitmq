using System;
using System.Threading.Tasks;
using RabbitMQ.Wrapper;

namespace PingerApp
{
    public class Pinger: IDisposable
    {
        private Wrapper wrapper;
        public Pinger(string userName, string password, string hostName, string queueName)
        {
            wrapper = new Wrapper( userName, password, hostName, queueName);
        }

        public async Task StartPingerAsync(string queueNameReceiver, string message, int delay)
        {
            wrapper.SendMessageToQueue(message, queueNameReceiver, 0);
            
            bool tmp = true;
            
            Task outer = Task.Run(() =>
            {
                Task inner = Task.Run(() =>
                {
                    while (tmp)
                    {
                        wrapper.ListenQueue();
                        wrapper.SendMessageToQueue(message, queueNameReceiver, delay);
                    }
                });
                ConsoleKeyInfo key;
                do
                {
                    key = Console.ReadKey(true);
                }
                while (key.Key != ConsoleKey.Escape);

                Console.WriteLine("The Escape key was pressed!");
                tmp = false;
            });
            await outer;
            outer.Wait();
        }
        
        public void Dispose()
        {
            wrapper?.Dispose(); 
        }
    }
}