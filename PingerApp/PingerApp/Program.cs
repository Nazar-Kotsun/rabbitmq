﻿using System;
using System.Threading.Tasks;

namespace PingerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Pinger pinger = new Pinger("root", "root", "localhost","ping_queue");

            Task t = pinger.StartPingerAsync("pong_queue", "ping", 2500);
            Task.WaitAll(t);
            
            pinger.Dispose();
        }
    }
}