﻿using System;
using System.Threading.Tasks;

namespace PongerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Ponger ponger = new Ponger("root", "root", "localhost","pong_queue");
           
            Task task = ponger.StartPongAsync("ping_queue", "pong", 2500);
            Task.WaitAll(task);
            
            ponger.Dispose();
        }
    }
}